{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<!------ Include the above in your HEAD tag ---------->

{* This tab show an upload form *}
<div class="panel">
	<h3><i class="icon process-icon-configure"></i> {l s='Banner Configuration' mod='op_promobanner'}</h3>

	<div class="row fluid" id="nav-upload" >

		<form enctype="multipart/form-data" class="col-8" id="form-upload-image" action="{$action_form}" method="POST">
			<div class="">
				<input type="hidden" name="submitCard" value="1">

				<div class="custom-file mb-3 col-6 row">

					<input type="file" class="custom-file-input" id="image-file" name="image-file" accept="image/*">
					<label class="custom-file-label" for="image-file">{l s='Open Image...' mod='op_promobanner'}</label>
				</div>

				<div class="input-group mb-3 col-6">
					<div class="input-group-prepend">
						<span class="input-group-text" id="product-name-label">{l s='Product Name' mod='op_promobanner'}</span>
						{$count=0}
						{foreach from=$langs item=lang}
							<div class="translatable-field col row lang-{$lang.id_lang}" {if $count!=0}style="display:none"{/if}{$count=$count+1}>
								<div class="col">
									<input type="text" class="form-control" name="product-name_{$lang.id_lang}" aria-describedby="product-name-label" aria-labelledby="product-name-label" style="height: auto" required onkeyup="if (isArrowKey(event)) return ;updateFriendlyURL();">
								</div>
								<div class="col-1">
									<button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown" aria-expanded="false">
										{$lang.iso_code}
									</button>
									<ul class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(5px, 31px, 0px); top: 0px; left: 0px; will-change: transform; overflow: visible" >
										{foreach from=$langs item=lang}
											<li><a href="javascript:hideOtherLanguage({$lang.id_lang});" tabindex="-1">{$lang.name}</a></li>
										{/foreach}
									</ul>
								</div>
							</div>
						{/foreach}
					</div>
				</div>
				<div class="input-group mb-3 col-6">
					<div class="input-group-prepend">
						<span class="input-group-text" for="product-link">{l s='Link' mod='op_promobanner'}</span>
						{$count=0}
						{foreach from=$langs item=lang}
							<div class="translatable-field col row lang-{$lang.id_lang}" {if $count!=0}style="display:none"{/if}{$count=$count+1}>
								<div class="col">
									<input type="url" class="form-control" name="product-link_{$lang.id_lang}" placeholder="https://your-domain.com/page/product" aria-describedby="product-price-label" style="height: auto" required onkeyup="if (isArrowKey(event)) return ;updateFriendlyURL();">
								</div>
								<div class="col-1">
									<button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown" aria-expanded="false">
										{$lang.iso_code}
									</button>
									<ul class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(5px, 31px, 0px); top: 0px; left: 0px; will-change: transform; overflow: visible" >
										{foreach from=$langs item=lang}
											<li><a href="javascript:hideOtherLanguage({$lang.id_lang});" tabindex="-1">{$lang.name}</a></li>
										{/foreach}
									</ul>
								</div>
							</div>
						{/foreach}
					</div>
				</div>
			</div>

			<button type="submit" role="submit" class="btn btn-primary pull-left" id="btn-upload-image" data-container="body" data-toggle="popover" data-placement="bottom" data-content="{l s='Mandatory fields in all languages' mod='op_promobanner'}">
				{l s='Upload and Save' mod='op_promobanner'}</button>
		</form>
		<div class="col-4 ">
			<div class="image-thumbnail justify-content-center p-3">
				<img class="border" src="http://placehold.it/250" id="image-thumbnail-file"  height="250px">
			</div>
		</div>

	</div>

</div>

{* This tab show a list of images*}
<div class="panel">
	<h3><i class="icon icon-list-ul"></i> {l s='Banner List' mod='op_promobanner'}</h3>
	<div id="list-banner">
		{$banner_list}
	</div>

</div>
