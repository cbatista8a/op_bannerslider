{if !empty($banners)}
<section id="op_bannerslider" style="margin:10px 0px">
        <div class="container">
            <div class="row flex-vertical-align">
                    <div class="col-lg-12 carosello-promo text-lg-center">
                    {foreach from=$banners item=banner key=key}
                        <div class="slick-item">
                            <a href="{$banner[$lang].link}">
                            <figure>
                            <img src="{$banner.image_url}" alt="{$banner[$lang].product_name}">
                                <figcaption class="caption">
                                    <h5 class="text-white title">{$banner[$lang].product_name}</h5>
                                </figcaption>
                            </figure>
                            </a>
                        </div>
                    {/foreach}
                    </div>
            </div>
        </div>
</section>
{/if}