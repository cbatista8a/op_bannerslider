/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/
window.onload= function(){
  bsCustomFileInput.init();
  //Text for Home Page
  $('#btn-home-text').on('click',function () {
    let url = document.getElementById('form-upload-image').action;
    let data = new FormData();
    data.append('submitHomeText','1');
    let home_text_fields = document.getElementsByTagName('textarea');
    $(home_text_fields).each(function () {
      data.append(this.name,this.value);
    });
    $.ajax({
      method: 'POST',
      url: url,
      data: data,
      processData: false,
      contentType: false,
      cache: false,
      success: function (response) {

      },
      error: function (response) {

      }
    }).done(function() {

    });
  });

  //On new image submit
  $('#form-upload-image').on('submit',function (e) {
    e.preventDefault();
    uploadImage(this);
  });

  //Prevent submit form if are fields empties
  $('#btn-upload-image').on('click',function (e) {
    let inputs = (this).form.querySelectorAll('input');
    $(inputs).each(function () {
      debugger;
        if (this.value == null || this.value == ''){
          e.preventDefault();
          $('#btn-upload-image').popover('show');
          setTimeout(function () {
            $('#btn-upload-image').popover('hide');
          },4000);

        }
    });

  });

  //On values update
  $('.btn-save-banner').on('click',function () {
    updateImage(this.form);
  });

  //On image delete
  $('.btn-delete-banner').on('click',function () {
    deleteImage(this.form);
  })

  //On refresh slider preview
  ;$('#btn-refresh-banner').on('click',function () {
    updatePreview();
  });


  /**
   * Load Img preview
   */
  $('#image-file').on('change',function () {
    let reader = new FileReader();
    reader.readAsDataURL(this.files[0]);
    reader.onload = function (e) {
      let image=new Image();
      image.src=e.target.result;
      image.onload = function () {
        document.getElementById('image-thumbnail-file').src=image.src;
      };
    };

  });


};


/**
 * Send video file to server
 * @param form
 */
function uploadImage(form) {
  let data = new FormData(form);
  $.ajax({
    method: 'POST',
    url: form.action,
    data: data,
    processData: false,
    contentType: false,
    cache: false,
    success: function (response) {
      form.reset();
      $('#image-thumbnail-file').attr('src','http://placehold.it/250');
      refreshBannerList(response);
    },
    error: function (response) {

    }
  }).done(function() {

  });
}

/**
 * Update Slider Preview
 */
function updatePreview() {
  let url = document.getElementById('form-upload-image').action;
  let data = new FormData();
  data.append('submitUpdatePreview','1');
  $.ajax({
    method: 'POST',
    url: url,
    data: data,
    processData: false,
    contentType: false,
    cache: false,
    success: function (response) {
      document.getElementById('slider-preview').innerHTML = response;
    },
    error: function (response) {

    }
  }).done(function() {

  });
}

/**
 * Update Card values
 * @param form
 */
function updateImage(form) {
  let data = new FormData(form);
  data.append('submit-btn-save-image','1');
  $.ajax({
    method: 'POST',
    url: form.action,
    data: data,
    processData: false,
    contentType: false,
    cache: false,
    success: function (response) {
      refreshBannerList(response);

    },
    error: function (response) {

    }
  }).done(function() {

  });
}

/**
 * Delete Card Image from banner carousel
 * @param form
 */
function deleteImage(form) {
  let data = new FormData(form);
  data.append('submit-btn-delete-image','1');
  $.ajax({
    method: 'POST',
    url: form.action,
    data: data,
    processData: false,
    contentType: false,
    cache: false,
    success: function (response) {
      refreshBannerList(response);
    },
    error: function (response) {

    }
  }).done(function() {

  });
}

/**
 * Refresh Image Card List and bind events
 * @param html
 */
function refreshBannerList(html) {
  debugger;
  document.getElementById('list-banner').innerHTML = html;
  $('.btn-save-banner').on('click',function () {
    updateImage(this.form);
  });

  $('.btn-delete-banner').on('click',function () {
    deleteImage(this.form);
  });
}